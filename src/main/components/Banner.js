import React from 'react';
import { Button,Icon,Modal } from 'antd';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import "./Banner.css"
import 'antd/dist/antd.css'
import pointPlot from 'point-plot'
import playBtnImg from "../../assets/home/bofang.png"


class Banner extends React.PureComponent{
    state={
        modalShow:false
    }

    componentDidMount(){
        pointPlot({
            canvas:document.getElementById('canvas'),
            color: '153,153,153', // default
            r: 3, // defalut
            distance: 150,
            isOnMove: false,
            isOnClick: true
        })
    }

    showVideo = ()=>{
        this.setState({
            modalShow:true
        })
    }
    hideVideo =()=>{
        this.setState({
            modalShow:false
        })
    }

    goProduct = (index) =>{
        const {onItemClick} = this.props
        onItemClick(index)
    }

    render(){
        const {modalShow} = this.state
        return(<div className="banner">
            <canvas id="canvas" style={{position:"fixed",height:'100vh',width:'70%',zIndex:'-1',top:0,right:0}}></canvas>
            {/*<video style={{height:'100vh',width:'100%',zIndex:'-1'}} autoPlay="autoPlay" src="https://sthg-images.oss-cn-beijing.aliyuncs.com/%E5%AE%A3%E4%BC%A0%E8%A7%86%E9%A2%91.mp4" loop="loop"  ></video>*/}
            <QueueAnim
                key="QueueAnim"
                type={['bottom', 'top']}
                delay={200}
            >
                <div className="banner-title" key="title">
                    捕捉全球信息 感知世界脉搏
                    <div className="banner-content" >
                        通过顶尖技术与服务，帮助客户抢占全球信息认知高地
                    </div>
                    <div className="know-more" key="knowMore">
                        <Button type="primary" shape="circle" icon="arrow-right" size="large" style={{background:'#214EF7'}} />
                        <span style={{marginLeft:20}}>了解详情</span>
                    </div>
                </div>
            </QueueAnim>
            <div>
                <QueueAnim key="item-show" type={['bottom','top']} delay={200} className="display-area">
                    <div className="display-area-item" key="black" onClick={()=>{this.goProduct(1)}}>
                        <span className="display-area-item-info">Obsidian</span>
                        <p style={{background:'#d8d8d8',width:30,height:2}}></p>
                        <span className="display-area-item-title">Plat X 黑曜</span>
                    </div>
                    <div className="display-area-item" key="yellow" onClick={()=>{this.goProduct(2)}}>
                        <span className="display-area-item-info">Amber</span>
                        <p style={{background:'#d8d8d8',width:30,height:2}}></p>
                        <span className="display-area-item-title">Plat X 琥珀</span>
                    </div>
                    <div className="display-area-item" key="green" onClick={()=>{this.goProduct(3)}}>
                        <span className="display-area-item-info">Iolite</span>
                        <p style={{background:'#d8d8d8',width:30,height:2}}></p>
                        <span className="display-area-item-title">Plat X 堇青</span>
                    </div>
                    <div className="display-area-item" key="bot" onClick={()=>{this.goProduct(4)}}>
                        <span className="display-area-item-info">Intelligent Robot</span>
                        <p style={{background:'#d8d8d8',width:30,height:2}}></p>
                        <span className="display-area-item-title">智能读写机器人</span>
                    </div>
                </QueueAnim>
            </div>
            <div className="play-button" onClick={this.showVideo}>
                <img src={playBtnImg} alt="播放视频" />
            </div>
            <TweenOne
                animation={{
                    y: '-=20',
                    yoyo: true,
                    repeat: -1,
                    duration: 1000,
                }}
                className="banner-icon"
                key="icon"
            >
                <Icon type="down" />
            </TweenOne>
            <Modal visible={modalShow} footer={null} width={700} onCancel={this.hideVideo} closable={false} destroyOnClose={true} bodyStyle={{padding:0,height:10}}>
                <video controls="controls" autoplay="autoPlay" src="https://sthg-images.oss-cn-beijing.aliyuncs.com/%E5%AE%A3%E4%BC%A0%E8%A7%86%E9%A2%91.mp4" style={{width:700}}></video>
            </Modal>
        </div>)
    }
}
export default Banner