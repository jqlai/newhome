import React from 'react'
import "./Location.css"
import map from '../../assets/home/地图.png'
class Location extends React.PureComponent{

    render(){
        return(
            <div className="Location">
                <div className="location-title">
                    <span>联系我们</span>
                    <div className="location-title-line"></div>
                </div>
                <div className="location-map">
                    <div className="location-map-content">
                       <img src={map} alt="地图"/>
                       <div className="location-map-info">
                           <div>
                           <span style={{fontSize:28}}>联系方式</span>
                           <div className="location-title-line" style={{width:80,background:'#fff'}}></div>
                           <p>北京市 海淀区中关村 768创意产业园</p>
                           <p>B座5号门1102</p>
                           <p>电话：010-62419934</p>
                           <p>邮箱：BD@stonehg.com</p>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Location