import React from 'react'

export default class Footer extends React.PureComponent {

    render() {
        return (
            <div style={{background:'#fff',padding:10}}>
                <p style={{color: '#C0BFBF',margin:0}}>版权所有 ©世通亨奇（北京）科技有限责任公司 保留一切权利 ICP17022676号 京ICP备17022676号-1
                    京公网安备11010802025434号</p>
            </div>
        )

    }
}