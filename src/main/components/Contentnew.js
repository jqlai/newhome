import React from 'react';
import { Row, Col } from 'antd';
import { TweenOneGroup } from 'rc-tween-one';
import ScrollAnim from 'rc-scroll-anim';
import "./Content.css"

class Contentnew extends React.PureComponent {
    getChildrenToRender = (data) =>
        data.map((item) => {
            return (
                <Col key={item.name} {...item}>
                    <div className="content-item" onClick={()=>{this.handleClick(item.id)}}>
                    <div className="content-item-img">
                    <img src={item.children.img.children} alt="金融" />
                    </div>
                    <div className="content-item-info">
                    <p className="content-item-info-title">{item.children.content.children}</p>
                    <p className="content-item-info-content">{item.children.content.content}</p>
                    </div>
                    </div>
                </Col>
            );
        });

    handleClick = (id)=>{
        const {itemClick} = this.props
        if(itemClick)itemClick(id)
    }

    render() {
        const { ...props } = this.props;
        const { dataSource } = props;
        delete props.dataSource;
        const childrenToRender = this.getChildrenToRender(
            dataSource.block.children
        );
        return (
            <div {...props} {...dataSource.wrapper}>
                <div {...dataSource.page}>
                    <ScrollAnim.OverPack
                        className={`content-template ${props.className}`}
                        {...dataSource.OverPack}
                    >
                        <TweenOneGroup
                            component={Row}
                            key="ul"
                            enter={{
                                y: '-=30',
                                opacity: 0,
                                type: 'from',
                                ease: 'easeInOutQuad',
                            }}
                            leave={{ y: '+=30', opacity: 0, ease: 'easeInOutQuad' }}
                            {...dataSource.block}
                        >
                            {childrenToRender}
                        </TweenOneGroup>
                    </ScrollAnim.OverPack>
                </div>
            </div>
        );
    }
}

export default Contentnew;
