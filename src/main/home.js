import React from 'react'
// import {Button} from 'antd'
import Banner from "./components/Banner"
import Content from "./components/Content"
import Contentnew from "./components/Contentnew"
import Location from "./components/Location"
import {ContentDataSource} from "../js/contentData";
import Footer from "./components/Footer"

class Home extends React.Component{


    componentDidMount(){
        window.scrollTo(0,0)
    }

    toProduct = ()=>{
        this.props.history.push("/product")
    }

    handleItemClick = (index) =>{
        this.props.history.push("/product",{select:index})
    }

    handlePlanClick = (index) =>{
        this.props.history.push("/plan",{select:index})
    }

    render(){
        console.log(this.props)
        return(
            <div>
               <Banner onItemClick={this.handleItemClick} />
                <Content/>
               <Contentnew dataSource={ContentDataSource} itemClick={this.handlePlanClick} />
                <Location />
                <Footer/>
            </div>
        )
    }
}

export default Home