export const productData = [
    {
        id:1,
        name:{
            ch:'黑曜',
            en:'Obsidian'
        },
        content:"面向开发者及企业提供基于事件机制，弹性、高可用、扩展性好、极速响应的全托管无服务器的计算能力。用户仅需关注自己业务代逻辑代码，无需关注服务器配置及资源，支持多种函数满足多样化需求。平台会根据计算需求智能调度资源，保证应用高效运行。",
        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/heiyao.png',
        block: {
            className: 'content5-img-wrapper',
            gutter: 128,
            type:'flex',
            children:[
                {
                    title:'AI基础平台',
                    info:'多算法框架、多设备兼容、多团队资源池共享，为AI应用开发提供一站式服务。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/pingtai.png'
                },
                {
                    title:'AI应用商店',
                    info:'成熟的算法模型，简单便捷的调用，加速AI应用开发。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/AI.png'
                },
                {
                    title:'FAAS',
                    info:'Serverless架构，减少开支，易于扩展，简化管理。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/kaas.png'
                },
                {
                    title:'KAAS',
                    info:'快速部署应用，无缝对接新的应用功能，优化硬件资源的使用。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/kaas.png'
                },
                {
                    title:'混合部署',
                    info:'多云以及私有化部署。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/hunhebushu.png'
                },

            ]
        },
    },
    {
        id:2,
        name:{
            ch:'琥珀',
            en:'Amber'
        },
        content: "将不同来源多种格式的各类型数据通过解析组件、文本抽取引擎、情感分析、音视频内容理解、关系抽取等方式，转换成可以进一步融合加工的知识。同时将知识组织为知识图谱，直接助力更多智能应用。",
        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/hupo.png',
        block: {
            className: 'content5-img-wrapper',
            gutter: 128,
            type:'flex',
            children:[
                {
                    title:'知识抽取',
                    info:'知识点提取以及知识关联，隐含关联关系分析，深度关系推理和隐含关系推理。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/zhishichouqu.png',
                },
                {
                    title:'本地构建',
                    info:'本地图谱构建，根据行业需求进行本地化部署。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/bendigoujian.png',
                },
                {
                    title:'可视化分析',
                    info:'多维度图谱可视化，多种算法集成。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/keshihua.png',
                },
                {
                    title:'图谱推理',
                    info:'语义理解、智能问答、语义搜索。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/tuputuili.png',
                },
                {
                    title:'数据融合',
                    info:'多源异构数据整合。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/shujuronghe.png',
                },

            ]
        },
    },
    {
        id:3,
        name:{
            ch:'堇青',
            en:'Iolite'
        },
        content:"产品可实现从全球各类的社交、电商、媒体等多种数据源采集用户所需信息存储到用户指定的位置，同时便于用于管理（筛选、查看、下载、删除）信息，支持实时或跨时段信息采集可适用于态势预警、舆情分析、行情预测等多种场景。",
        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/jinqing.png',
        block: {
            className: 'content5-img-wrapper',
            gutter: 128,
            type:'flex',
            children:[
                {
                    title:'全球分布式爬取',
                    info:'支持就近多语种的高效采集方式计算资源弹性扩、缩容。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/quanqiufenbupaqu.png',
                },
                {
                    title:'智能提取',
                    info:'对任何网页进行智能识别、分析和结构化数据提取。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/zhinengtiqu.png',
                },
                {
                    title:'安全采集',
                    info:'针对不同采集源，提供海量IP池账号池多种爬取服务。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/anquan.png',
                },
                {
                    title:'专业数据集市',
                    info:'相关专业领域积累大量历史数据  可在数据仓库下载。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/zhanye.png',
                },
                {
                    title:'简单易用',
                    info:'可视化管理并支持多种数据导出和推送，支持API调用。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/jiandan.png',
                },
                {
                    title:'灵活交付',
                    info:'支持本地化部署以及云端采集服务。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/linghuo.png',
                },
            ]
        },
    },
    {
        id:4,
        name:{
            ch:'智能读写机器人',
            en:'Intelligent Robot'
        },
        content:"运用机器学习和自然语言处理来自动分析大型开源数据。构建智能系统，可以读取文档，发现洞察力并自动生成与业务专家分析师级别的报告。",
        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/jiqiren.png',
        block: {
            className: 'content5-img-wrapper',
            gutter: 128,
            type:'flex',
            children:[
                {
                    title:'全维度分析',
                    info:'基于所有数据与自主学习，不断更新分析结果。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/weidu.png',
                },
                {
                    title:'跨领域理解',
                    info:'夸领域分析相关线索并融合，探索大事件背后的相关联系。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/kuajie.png',
                },
                {
                    title:'情报自动生成',
                    info:'负责提取、识别信息，大规模分析输出生成为可读的文本和图形',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/qingbao.png',
                },
                {
                    title:'不断学习',
                    info:'根据反馈与分析结果，不断改进与学习分析能力。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/xuexi.png',
                },
                {
                    title:'实时更新',
                    info:'随着全球的事件变化而更新，报告即时可用。',
                    img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/gengxin.png',
                }

            ]
        },
    }
]