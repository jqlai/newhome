export const ContentDataSource = {
    wrapper: { className: 'home-page-wrapper content5-wrapper' },
    page: { className: 'home-page content5' },
    OverPack: { playScale:0.3,className: '' },
    titleWrapper: {
        className: 'title-wrapper',
        children: [
            { name: 'title', children: '客户案例', className: 'title-h1' },
            {
                name: 'content',
                className: 'title-content',
                children: '在这里用一段话介绍服务的案例情况',
            },
        ],
    },
    block: {
        className: 'content5-img-wrapper',
        gutter: 16,
        children: [
            {
                id:1,
                name: 'block0',
                className: 'block',
                md: 8,
                xs: 24,
                children: {
                    wrapper: { className: 'content5-block-content' },
                    img: {
                        children:
                            'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/jinrong.png',
                    },
                    content: { children: '金融领域',content:'致力于围绕金融行业二级市场精准收集互联网开源数据' },
                },
            },
            {
                id:2,
                name: 'block1',
                className: 'block',
                md: 8,
                xs: 24,
                children: {
                    wrapper: { className: 'content5-block-content' },
                    img: {
                        children:
                            'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/lingshou.png',
                    },
                    content: { children: '零售领域',content:'获取互联网开放电商行业数据并合淘宝饿了么等大型电商平台' },
                },
            },
            {
                id:3,
                name: 'block2',
                className: 'block',
                md: 8,
                xs: 24,
                children: {
                    wrapper: { className: 'content5-block-content' },
                    img: {
                        children:
                            'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/zhengqi.png',
                    },
                    content: { children: '政企舆情',content:'利用智能爬取、人工智能、云计算等技术提供全网多渠道信息检测与预警' },                },
            },
            {
                id:4,
                name: 'block3',
                className: 'block',
                md: 8,
                xs: 24,
                children: {
                    wrapper: { className: 'content5-block-content' },
                    img: {
                        children:
                            'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/qingbao.png',
                    },
                    content: { children: '情报领域',content:'匹配情报业务需求，精准检索互联网开源帐号及提取相关信息' },
                },
            },
            {
                id:5,
                name: 'block4',
                className: 'block',
                md: 8,
                xs: 24,
                children: {
                    wrapper: { className: 'content5-block-content' },
                    img: {
                        children:
                            'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/yiliao.png',
                    },
                    content: { children: '医疗领域',content:'为国家医管部门打造基于大数据科学体系的医疗评价体系' },
                },
            },
        ],
    },
};