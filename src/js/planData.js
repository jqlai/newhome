export const planData = {
    OverPack: { playScale:0.3,className: '' },
    plan:[
        {
            id:1,
            name:{
              ch:'金融领域',
              en:'Financial field'
            },
            content:"致力于围绕金融行业二级市场，精准收集相关互联网开源数据金融咨询、机构近期动态结合行业机构分析报告等，运用大数据、知识图谱、人工智能等技术实现金融机构债券信用智能评级、高频商品期货智能快速交易 。为金融环境良好、普惠金融的发展贡献科技力量。",
            img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/plan/jinronglingyu.png',
            pic:"https://sthg-images.oss-cn-beijing.aliyuncs.com/official/example/%E9%87%91%E8%9E%8D%E6%A1%86%E6%9E%B6.png",
            block:{
                className: 'content5-img-wrapper',
                gutter: 128,
                type:'flex',
                children:[
                    {
                        title:'降低运维成本',
                        info:'借助于海量容器池与智能调度功能，无需额外搭建服务器，节省企业运维成本。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/chengben.png',
                    },
                    {
                        title:'安全稳定',
                        info:'丰富的API接口能力，节省开发成本，专注业务领域创新。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/anquan.png',
                    },
                    {
                        title:'数据准确全面',
                        info:'数据来源更丰富，涵盖海内外信息源，同时支持定制化选择数据来源。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/jingzhun.png',
                    }
                ]
            },
            example:{
                title:'某基金公司',
                content1:"某基金公司希望能够在他们的日常交易中引入人工智能算法自动交易。",
                content2:"在我们的解决方案中，我们提供了高频交易人工智能，通过迅速且大规模的频繁交易在价格的轻微波动中盈利。系统可以通过对几十亿条实盘和模拟盘的深层强化学习总结经验，用最快的速度和最优的价格来执行指令。",
                img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/jinrong.png'
            }
        },
        {
            id:2,
            name:{
                ch:'零售领域',
                en:'Retail sector'
            },
            content:"获取互联网开放电商行业数据，并和淘宝、饿了么等大型电商平台及平台入驻商户，进行三方合作，授权获取商户内部运营数据，进行深度数据融合，帮助商家进行智能化数据运营（例如：通过评价数据优化菜品口味，提升餐厅口碑；利用同行商圈数据比对，进行价格、商品策略调整等）。",
            img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/plan/lingshoulingyu.png',
            pic:"https://sthg-images.oss-cn-beijing.aliyuncs.com/official/example/%E9%9B%B6%E5%94%AE%E6%A1%86%E6%9E%B6.png",
            block:{
                className: 'content5-img-wrapper',
                gutter: 128,
                type:'flex',
                children:[
                    {
                        title:'海量商业信息',
                        info:'获取互联网开放电商行业数据，并和淘宝、饿了么等大型电商平台 及平台入驻商户进行三方合作授权获取商户内部运营数据。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/zhanye.png',
                    },
                    {
                        title:'商业化信息输出',
                        info:'对所有商业信息进行深度数据融合，利用同行商圈数据比对，进行价格、商品策略调整等，帮助商家进行智能化数据运营。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/shuchu.png',
                    },
                    {
                        title:'高效简易',
                        info:'可提高效率，快速搭建业务系统开发、测试、部署上云。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/jiandan.png',
                    },
                ]
            },
            example:{
                title:'电商商家',
                content1:"电商商家因为已经入驻各类平台，所以自己店铺其实有海量的数据可以获取，然后大部分商家并不知道应当如何发挥自身数据的最大价值依然采用传统的运营方式管理店铺。",
                content2:"在我们的解决方案中，对于多来源的数据，我们利用人工智能算法做了大量的处理，直接给商家提供了口碑提升，价格策略调整等简单易用的功能，产品广受好评，累计服务商家3万家以上。",
                img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/lingshou.png'
            }
        },
        {
            id:3,
            name:{
                ch:'政企舆情',
                en:'Financial field'
            },
            content:"利用智能爬取、人工智能、云计算等技术，提供全网多渠道信息的监测与预警、重大或突发舆情事件的深度分析与研判、舆情疏导与效果评估等一站式舆情业务闭环智能服务。为海内外政府和企业在情报收集、舆情应对、业务决策等方面提供有力技术支撑。",
            img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/plan/zhengqilingyu.png',
            pic:"https://sthg-images.oss-cn-beijing.aliyuncs.com/official/example/%E6%94%BF%E4%BC%81%E6%A1%86%E6%9E%B6.png",
            block:{
                className: 'content5-img-wrapper',
                gutter: 128,
                type:'flex',
                children:[
                    {
                        title:'信息源准确',
                        info:'借助于多地区数据采集部署进准索取海量事件信息、经纬等事件内容。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/jingzhun.png',
                    },
                    {
                        title:'精准知识分析',
                        info:'结合历史积累情报数据，深度挖掘数据关联关系，并将海量多源异构 数据实时转化为“人事地物组织”，不断与新数据关联分析精准输出。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/tuputuili.png',
                    },
                    {
                        title:'稳定可靠',
                        info:'数据来源更丰富，涵盖海内外信息源，同时支持定制化选择数据来源。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/anquan.png',
                    },
                ]
            },
            example:{
                title:'某大型国企',
                content1:"某大型国企对企业的负面舆情高度关注，在使用以往的舆情处理系统时常常会遇到漏报负面舆情以及无法预判舆情走向的问题。",
                content2:"在我们的解决方案中，我们对与该企业相关的国内国外新闻媒体社交媒体的信息都加以了实时监控，响应时间在15分钟内，漏报率0%。同时对单条舆情的走势脉络，传播方向都给予了详尽且清晰的分析帮助企业的公关部门的公关部门迅速决定是否要处理、如何处理、从何处入手等。大大提升了公关部门的运营效率。",
                img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/zhengqi.png'
            }
        },
        {
            id:4,
            name:{
                ch:'情报领域',
                en:'Intelligence field'
            },
            content:"匹配情报业务需求，精准检索互联网开源账号及提取相关信息，结合历史积累情报数据，深度挖掘数据关联关系，并将海量多源异构数据实时转化为“人事地物组织”数据编织情报领域专属的知识图谱，结合机器学习技术和业务专家经验打造行业人工智能，有力支撑情报部门开展情报研判分析、重要人物动态追踪、重大事件预警、情报报文生成等重要实战工作。",
            img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/plan/qingbaolingyu.png',
            pic:"https://sthg-images.oss-cn-beijing.aliyuncs.com/official/example/%E6%83%85%E6%8A%A5%E6%A1%86%E6%9E%B6.png",
            block:{
                className: 'content5-img-wrapper',
                gutter: 128,
                type:'flex',
                children:[
                    {
                        title:'多维分析',
                        info:'深度分析数据关联关系，结合机器学习，不断完善领域时间分析并输出。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/weidu.png',
                    },
                    {
                        title:'海量数据',
                        info:'对部分领域的情报数据长期积累拥有海量数据池，可以直接进行数据提取。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/zhanye.png',
                    },
                    {
                        title:'信息源准确',
                        info:'借助于多地区数据采集部署进准索取海量事件信息、经纬等事件内容。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/jingzhun.png',
                    },
                ]
            },
            example:{
                title:'国家重要部门',
                content1:"国家重要部门需要研究关于国际某大事件的情报，并系统地汇集成报告提供给相关部门，辅助决策。以前的做法是运用大量人力，通过搜索引擎等方式去各处搜集相关资料，再交由多个业务专家花大量时间逐篇阅读，最后写成报告。",
                content2:"在我们的解决方案中，用户设定研究专题后，系统通过实体识别自动推荐相关关键词以及信息来源。用户选定关键词和信息来源后，系统会自动遍历公开历史数据并且24小时获取最新相关信息，对于获取到的信息，系统可以进行概要总结并对其中的各个实体对应的资料和关联作可视化展示，极大地提高了用户阅读理解信息的速度。根据用户实际反映，相同工作的耗时量减少了80%以上。",
                img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/qingbao.png'
            }
        },
        {
            id:5,
            name:{
                ch:'医疗领域',
                en:'Medical field'
            },
            content:"围绕国家医疗管理部门需求，以医院为主体单位，进准采集关联数据，并构建医院知识图谱，包含医院概况、动态新闻、专家力量、科研水平、患者纠纷等多维度数据，实时高效掌握医院状况，并结合内部满意度调查数据（数千万患者参与），为国家医管部门打造基于大数据科学体系的医疗评价体系。",
            img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/plan/yiliaolingyu.png',
            pic:"https://sthg-images.oss-cn-beijing.aliyuncs.com/official/example/%E5%8C%BB%E7%96%97%E6%A1%86%E6%9E%B6.png",
            block:{
                className: 'content5-img-wrapper',
                gutter: 128,
                type:'flex',
                children:[
                    {
                        title:'精准信息提取',
                        info:'通过智能分析，高效、精准提取多维度地方医疗与患者相关信息数据；无需调整数据存储环境，通过云容器，即可存取海量数据',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/jingzhun.png',
                    },
                    {
                        title:'智能知识输出',
                        info:'结合海量医患信息，深度挖掘数据关联关系，并将多源数据转化为“人事地物”数据，结合机器学习，输出重要理疗研判分析。',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/shuchu.png',
                    },
                    {
                        title:'调动方案多',
                        info:'借助于医疗知识图谱的构建相关医管部门能够实现数据级地区医疗状态梳理，还可通过借助对云管数据，对地方医疗体系进行指导与调整',
                        img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/blocks/kuajie.png',
                    },
                ]
            },
            example:{
                title:'某基金公司',
                content1:"某医疗管理部门需要统一管理大量医院的多维度数据从而高效掌握各个医院情况，以前由于各个医院的数据不好统一，结构化半结构化数据参差不齐，一直没有很好地管理起来。",
                content2:"在我们的解决方案中，我们对于多源异构数据做了很好的整合，基于我们高效的大数据云管平台，精准的人工智能语义分析模型，给该部门的医疗评价体系带来了极大的提升，评价数据得以全面直观地展现在管理人员面前。",
                img:'https://sthg-images.oss-cn-beijing.aliyuncs.com/official/home/yiliao.png'
            }
        },
    ]
}