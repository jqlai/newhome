import React from "react"
import NavHeader from '../product/components/NavHeader'
import Explain from './components/Explain'
import PlanBolck from "./components/PlanBolck"
import Deploy from "./components/Deploy"
import Example from "./components/Example"
import {planData} from "../js/planData";
import {Anchor} from "antd";

const Link = Anchor.Link

export default class Plan extends React.PureComponent{

    state = {
        plan:planData.plan[0],
        select:1
    }

    componentDidMount(){
        window.scrollTo(0,0)
    }

    changeItem = (index)=>{
        this.setState({
            plan:planData.plan[index-1]
        })
    }
    render(){
        const dataSource = planData.plan
        const {select,plan} = this.state
        const {location:{state}} = this.props
        let selectIndex = select
        if(state&&state.select){
            selectIndex = state.select
        }
        return(
            <div>
                <Anchor style={{position:"fixed",top:400,left:100,background:'none'}} offsetTop={400}>
                    <Link href="#Explain" title="方案说明"/>
                    <Link href="#PlanBolck" title="方案特性"/>
                    <Link href="#Deploy" title="部署架构"/>
                    <Link href="#Example" title="客户案例"/>
                </Anchor>
                <NavHeader dataSource={dataSource} onItemChange={this.changeItem} selectIndex={selectIndex} />
                <Explain plan={plan} />
                <PlanBolck showData={plan}/>
                <Deploy showData={plan}/>
                <Example showData={plan}/>
            </div>
        )
    }
}