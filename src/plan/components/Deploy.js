import React from "react"


export default class Deploy extends React.PureComponent{

    render(){
        const {showData} = this.props
        return(
            <div id="Deploy" style={{marginTop:40}}>
                <div className="ProductBlock-title">
                    部署架构
                </div>
                <div style={{maxWidth:1200,margin:'auto'}}>
                  <img src={showData.pic} alt="部署架构" style={{width:'100%'}}/>
                </div>
            </div>
        )
    }

}