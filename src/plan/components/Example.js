import React from "react"
import "./Example.css"


export default class Example extends React.PureComponent{

    render(){
        const {showData} = this.props
        return(
            <div id="Example" className="Example">
                <div className="ProductBlock-title">
                    客户案例
                </div>
                <div className="Example-Item">
                    <div className="Example-Item-info">
                        <div className="Example-Item-info-title">{showData.example.title}</div>
                        <div className="Example-Item-info-content">
                          <p className="Example-Item-info-content-title">项目背景</p>
                          <p>{showData.example.content1}</p>
                        </div>
                        <div className="Example-Item-info-content">
                          <p className="Example-Item-info-content-title">解决方案</p>
                          <p>{showData.example.content2}</p>
                        </div>
                    </div>
                    <div className="Example-Item-img">
                        <img src={showData.example.img} alt="案例图片"/>
                    </div>
                </div>
            </div>
        )
    }

}