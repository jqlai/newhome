import React from "react"
import "./Explain.css"

export default class Explain extends React.PureComponent{

    render(){
        const {plan} = this.props
        return(
            <div className="Explain" id="Explain">
                <div className="Explain-content">
                    <p className="Explain-content-title">方案说明</p>
                    <p className="Explain-content-content">{plan.content}</p>
                </div>
                <div className="Explain-img">
                    <img src={plan.img} alt="说明图片" />
                </div>
            </div>
        )
    }

}