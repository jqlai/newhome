import React from 'react'
import NavHeader from './components/NavHeader'
import ProductContent from "./components/ProductContent"
import ProductBlock from "./components/ProductBlock"
import Footer from "../main/components/Footer"
import {productData} from "../js/productData";

class Product extends React.Component{
    state = {
        product:productData[0],
        select:1
    }

    componentDidMount(){
        window.scrollTo(0,0)
    }

    changeItem = (index) => {
        this.setState({
            product:productData[index-1]
        })
    }

    render(){
        const {select,product} = this.state
        const {location:{state}} = this.props
        let selectIndex = select
        if(state&&state.select){
            selectIndex = state.select
        }
        return(
            <div>
                <NavHeader dataSource={productData} onItemChange={this.changeItem} selectIndex={selectIndex} />
                <ProductContent showData={product} />
                <ProductBlock showData={product}/>
                <Footer />
            </div>
        )
    }
}
export default Product