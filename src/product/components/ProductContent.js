import React from 'react'
import {Button} from 'antd'
import "./ProductContent.css"
import TweenOne from "rc-tween-one";


export default class ProductContent extends React.PureComponent{
    render() {
        const {showData} = this.props
        return (<div className="ProductContent">
            <div className="ProductContent-title">
                <p>产品概述</p>
            </div>
            <div className="ProductContent-content">
                <TweenOne
                    animation={{
                        x: '+=40',
                        yoyo: true,
                        duration: 1000,
                    }}
                    key={"content"+showData.id}
                >
                    <div className="ProductContent-content-info">
                        <div className="ProductContent-content-info-title">
                            <p className="ProductContent-content-info-title-ch">{showData.name.ch}</p>
                            <p className="ProductContent-content-info-title-en">{showData.name.en}</p>
                        </div>
                        <div className="ProductContent-content-info-content">
                            <p>{showData.content}</p>
                        </div>
                        <div className="ProductContent-content-info-button">
                            <Button style={{marginRight:30,borderColor:'#214EF7',background:'#214EF7'}} type="primary">立即使用</Button>
                            <Button style={{color:'#214EF7',borderColor:'#214EF7'}}>使用文档</Button>
                        </div>
                    </div>
                </TweenOne>
                <TweenOne
                    animation={{
                        x: '-=40',
                        yoyo: true,
                        duration: 1000,
                    }}
                    key={"icon"+showData.id}
                >
                    <div className="ProductContent-content-img">
                        <img src={showData.img} alt="产品图片" />
                    </div>
                </TweenOne>
            </div>
        </div>)
    }

}