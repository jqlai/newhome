import React from 'react'
import "./NavHeader.css"

export default class NavHeader extends React.PureComponent{

    state = {
        select:1
    }

    componentDidMount(){
        const {selectIndex} = this.props
        if(selectIndex){
            this.selectItem(selectIndex)
        }
    }

    selectItem = (item) => {
        const {onItemChange} = this.props
        this.setState({
            select:item
        })
        onItemChange(item)
    }

    render(){
        const {dataSource} = this.props
        const {select} = this.state
        const menu = dataSource?dataSource.map(e=>(
            <div className="NavHeader-item" onClick={()=>{this.selectItem(e.id)}} style={select===e.id?{height:200,paddingTop:100,borderColor:'#214EF7'}:null} key={e.id}>
                <p style={select===e.id?{color:'#214EF7'}:null}>{e.name.ch}</p>
                <p style={select===e.id?{color:'#214EF7'}:{color:'#d8d8d8',margin:0}}>{e.name.en}</p>
            </div>
        )):null
        return(
            <div className="NavHeader">
                {menu}
            </div>
        )
    }
}