import React from 'react'
import "./ProductBlock.css"
import ScrollAnim from "rc-scroll-anim";
import { TweenOneGroup } from 'rc-tween-one';
import {Row,Col} from "antd";

export default class ProductBlock extends React.PureComponent{


    render(){
        const {showData} = this.props
        const childrenCol = showData.block&&showData.block.children?showData.block.children.map(e=>{
            return(
                <Col md={8} xs={24} key={e.title}>
                    <div className="ProductBlock-item">
                        <div className="ProductBlock-item-img">
                            <img src={e.img} alt="功能" />
                        </div>
                        <div className="ProductBlock-item-content">
                            <p className="ProductBlock-item-content-title">
                                {e.title}
                            </p>
                            <p className="ProductBlock-item-content-content">
                                {e.info}
                            </p>
                        </div>
                    </div>
                </Col>
            )
        }):[]
        return(
            <div className="ProductBlock">
                <div className="ProductBlock-title">
                    产品优势
                </div>
                <div className="ProductBlock-content">
                    <ScrollAnim.OverPack
                        className={`content-template ${showData.id}`}
                        playScale={0.3}
                    >
                        <TweenOneGroup
                            component={Row}
                            key="product-ul"
                            enter={{
                                y: '+=30',
                                opacity: 0,
                                type: 'from',
                                ease: 'easeInOutQuad',
                            }}
                            leave={{ y: '+=30', opacity: 0, ease: 'easeInOutQuad' }}
                            {...showData.block}
                        >
                            {childrenCol}
                        </TweenOneGroup>
                    </ScrollAnim.OverPack>
                </div>
            </div>
        )
    }

}