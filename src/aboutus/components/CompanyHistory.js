import React from "react"
import "./CompanyHistory.css"
import {Timeline} from "antd"

export default class CompanyHistory extends React.PureComponent{

    render() {
        return (
            <div className="CompanyHistory" id="CompanyHistory">
                <div className="CompanyHistory-title">
                    企业历程
                </div>
                <div className="CompanyHistory-content">
                    <Timeline mode="alternate">
                        <Timeline.Item color="white">
                            <p className="timeline-title">2016</p>
                            <p className="timeline-content">
                                10月 取得阿里ISV资质，具备直接获取电商核心数据的能力
                            </p>
                            <p className="timeline-content">
                                08月 成为饿了么战略合作伙伴，在饿了么服务市场推出首款大数据运营软件服务
                            </p>
                        </Timeline.Item>
                        <Timeline.Item color="white">
                            <p className="timeline-title">2017</p>
                            <p className="timeline-content">08月 与中科院电子所苏州研究院达成战略合作关系</p>
                            <p className="timeline-content">04月 公司取得多项软件著作权证书</p>
                            <p className="timeline-content">02月 入围国家重要部门软件及数据解决方案招标采购</p>
                        </Timeline.Item>
                        <Timeline.Item color="white">
                            <p className="timeline-title">2018</p>
                            <p className="timeline-content">12月 完成首轮融资，由中科院创投领投</p>
                            <p className="timeline-content">11月 platx堇青1.0,数据采集平台上线</p>
                            <p className="timeline-content">
                                09月 与中科院电子所达成战略合作关系
                            </p>
                            <p className="timeline-content">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;与卫健委医管中心建立合作关系</p>
                            <p className="timeline-content">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;与工信部通信管理局建立合作关系</p>
                        </Timeline.Item>
                        <Timeline.Item></Timeline.Item>
                    </Timeline>
                </div>
            </div>
        )
    }

}