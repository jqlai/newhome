import React from "react"
import "./Team.css"
import {Row,Col} from "antd"

export default class Team extends React.PureComponent{

    render() {
        return (
            <div className="Team" id="Team">
                <div className='Team-title'>
                    团队介绍
                </div>
                <div className="Team-content">
                    <Row gutter={128}>
                        <Col md={12} xs={24}>
                            <div className="Team-content-item" >
                                <div className="Team-content-item-photo">
                                    <img src="https://t.alipayobjects.com/images/rmsweb/T11aVgXc4eXXXXXXXX.svg" alt="logo"/>
                                </div>
                                <div className="Team-content-item-info">
                                    <p className="Team-content-item-info-name">黄宇·CEO</p>
                                    <p className="Team-content-item-info-content">
                                        中国科学院电子所博士、研究员、硕导，青促会成员，国家科技进步一等奖，中科院杰出成就奖获得者，师从信息领域权威专家吴一戎院士，领导国家级（经费过亿）系统研制建设多项。
                                    </p>
                                </div>
                            </div>
                        </Col>
                        <Col md={12} xs={24}>
                            <div className="Team-content-item" >
                                <div className="Team-content-item-photo">
                                    <img src="https://t.alipayobjects.com/images/rmsweb/T11aVgXc4eXXXXXXXX.svg" alt="logo"/>
                                </div>
                                <div className="Team-content-item-info">
                                    <p className="Team-content-item-info-name">王义成·销售总监</p>
                                    <p className="Team-content-item-info-content">
                                        英国华威大学计算机应用硕士毕业，现就读长江商学院。中科院电子所工作经验期间参与多项国家重大平台建设，从0到1打造TO B数据产品服务，累计服务数万商家。
                                    </p>
                                </div>
                            </div>
                        </Col>
                        <Col md={12} xs={24}>
                            <div className="Team-content-item" >
                                <div className="Team-content-item-photo"></div>
                                <div className="Team-content-item-info">
                                    <p className="Team-content-item-info-name">杨志锋·项目总监</p>
                                    <p className="Team-content-item-info-content">
                                        中国科学院电子所博士，长期致力于为客户提供IT战略规划相关解决方案的咨询工作，具有超过10年的国家级项目管理经验。
                                    </p>
                                </div>
                            </div>
                        </Col>
                        <Col md={12} xs={24}>
                            <div className="Team-content-item" >
                                <div className="Team-content-item-photo"></div>
                                <div className="Team-content-item-info">
                                    <p className="Team-content-item-info-name">孔祥博·产品总监</p>
                                    <p className="Team-content-item-info-content">
                                        北京邮电大学通信本科，英国华威大学计算机硕士，5年IBM咨询顾问，参与IBM大中华区认知计算（Watson）产品设计建设与项目实施。
                                    </p>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }

}