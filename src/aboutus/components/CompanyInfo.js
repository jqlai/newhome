import React from 'react'
import "./CompanyInfo.css"

export default class CompanyInfo extends React.PureComponent{

    render(){
        return(
            <div className="CompanyInfo" id="CompanyInfo">
                <div className="CompanyInfo-title">
                    公司简介
                </div>
                <div className="CompanyInfo-info">
                    <div className="CompanyInfo-info-info">
                    <p className="CompanyInfo-info-title">
                        世通亨奇
                    </p>
                    <p className="CompanyInfo-info-content">
                        世通亨奇（北京）科技有限公司，是一家创新型的数据智能解决方案服务商。核心团队来自中国科学院、IBM、微软、思杰、帝国理工等国内外知名科研院所或企业。团队长期以来致力于运用人工智能（AI）及云计算等相关技术，对互联网公开大数据进行感知与获取、建模与解析、管理与决策，并在这三个方面，取得实质性突破，成功打造出国内领先的技术产品解决方案PLAT-X，服务于军工、政企、金融、零售等重点领域及行业
                    </p>
                    </div>
                    <div className="CompanyInfo-img">
                        <img src="https://t.alipayobjects.com/images/rmsweb/T11aVgXc4eXXXXXXXX.svg" alt="logo"/>
                    </div>
                </div>
            </div>
        )
    }
}