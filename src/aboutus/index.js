import React from "react"
import CompanyInfo from "./components/CompanyInfo"
import CompanyHistory from "./components/CompanyHistory"
import Team from "./components/Team"
import { Anchor } from 'antd';

const { Link } = Anchor;

export default class AboutUs extends React.PureComponent{
    componentDidMount(){
        window.scrollTo(0,0)
    }
    render(){
        return(
            <div>
                <Anchor style={{position:"fixed",top:400,left:100,background:'none'}} offsetTop={200}>
                    <Link href="#CompanyInfo" title="公司简介"/>
                    <Link href="#CompanyHistory" title="企业历程"/>
                    <Link href="#Team" title="团队介绍"/>
                </Anchor>
                <CompanyInfo />
                <CompanyHistory />
                <Team />
            </div>
        )
    }
}