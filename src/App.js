import React from 'react';
import './App.css';
import {BrowserRouter as Router,Route,Link} from 'react-router-dom'
import {Icon} from "antd"
import MenuImg from './assets/home/menu.png'
import logo from "./assets/home/logo.png"
import Home from "./main/home";
import Product from "./product/index"
import AboutUs from "./aboutus/index"
import Plan from "./plan/index"

class App extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            menushow:false,
            select:1
        }
    }

    menuShow = ()=>{
        this.setState({
            menushow:true
        })
    }
    closeMenu = ()=>{
        this.setState({
            menushow:false
        })
    }
    selectItem = (index)=>{
        this.setState({
            select:index
        })
    }

    render(){
        const {menushow} = this.state
        console.log(this)
        return (
            <div className="App">
                <Router>
                    <div style={{position:'fixed',left:50,top:100,display:'flex',zIndex:1000}}>
                        <div style={{marginRight:48,cursor: 'pointer'}} onClick={this.menuShow}><img src={MenuImg} alt="菜单" /></div>
                        <div style={{cursor: 'pointer'}}>
                            <Link to="/"><img src={logo} alt="logo" /></Link>
                        </div>
                        {menushow?<div className="Menu">
                            <div className="Menu-Item" onClick={()=>{this.selectItem(1)}}>
                                <Link to="/"><span>首页</span></Link>
                            </div>
                            <div className="Menu-Item" onClick={()=>{this.selectItem(2)}}>
                                <Link to="/product"><span>产品服务</span></Link>
                            </div>
                            <div className="Menu-Item" onClick={()=>{this.selectItem(3)}}>
                                <Link to="/plan"><span>解决方案</span></Link>
                            </div>
                            <div className="Menu-Item" onClick={()=>{this.selectItem(4)}}>
                                <span>服务与支持</span>
                            </div>
                            <div className="Menu-Item" onClick={()=>{this.selectItem(5)}}>
                                <Link to="/about"><span>关于我们</span></Link>
                            </div>
                            <div className="Close-Button" onClick={this.closeMenu}>
                                <Icon type="close" />
                            </div>
                        </div>:null}
                    </div>
                    <Route exact path="/" component={Home} />
                    <Route path="/product" component={Product} />
                    <Route path="/plan" component={Plan}/>
                    <Route path="/about" component={AboutUs} />
                </Router>
            </div>
        );
    }
}

export default App;
